// get the client
import mysql from'mysql2'

// Create the connection pool. The pool-specific settings are the defaults
const pool = mysql.createPool({
  host: '0.0.0.0',
  user: 'TIF',
  password:'tif',
  database: 'tifProject',
  waitForConnections: true,
  connectionLimit: 20,
  queueLimit: 0
});

export default pool;