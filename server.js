// const express = require('express')
import express from 'express'
import bodyParser from 'body-parser'
import cors  from 'cors'
import jwt from 'jsonwebtoken'
import cookieParser from 'cookie-parser'

import secret from './config.js'

// Express
const app = express()


// Routers
import roleRouter from './v1/role.js'
import userRouter from './v1/user.js'
import communityRouter from './v1/community.js'
import memberRouter from './v1/member.js'

//middleware
app.use(cors('*'))
app.use(bodyParser.json())
app.use(cookieParser())

app.use((request, response, next) => {
    if (request.url == '/v1/auth/signin' ||
        request.url == '/v1/auth/signup') {
        // do not check for token/cookie
        next()
    } else {

        try {
            // cookie based authorization system instead of Authorization header

            const token = request.cookies.token
            // console.log(request.cookies.token)
            const data = jwt.verify(token, secret)

            // add a new key named userId with logged in user's id
            request.id = data['id']

            // go to the actual route
            next()

        } catch (ex) {
            response.status(401)
            response.send({ status: 'error', error: 'protected api', hint: 'redirect to /v1/auth/signin' })
        }

    }
})


// Use Routers
app.use('/v1/role', roleRouter);
app.use('/v1', userRouter);
app.use('/v1/community', communityRouter);
app.use('/v1/member', memberRouter);

app.get('/', (request, response) => {
    response.send('In main server')
})



app.listen(5000, '0.0.0.0', () => {
    console.log('Server started on port 5000')
})