const utils = {
      
    createError: function createError(error) {
        const result = {};
        result['status'] = 'false'
        result['error'] = error
        return result
    },
      
      
    createMetaSuccess: function createMetaSuccess(data) {
        const result = {};
        result ['status'] = 'true'
        result ['content'] = {
            meta: {
                total : data.length,
                pages : 1,
                page : 1
            },
            data: data
        }
        return result
    },

    createSuccess: function createSuccess(data) {
        const result = {};
        result ['status'] = 'true'
        result ['content'] = {data: data}
        return result
    }
}

export default utils