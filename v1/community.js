import express from 'express'
import Snowflake from '@theinternetfolks/snowflake'
import utils from '../utils.js'
import db from '../db.js'


const router = express.Router()

// ---------------------------------------
//                  POST
// ---------------------------------------

// To create Community

router.post('/', (request, response) => {
    const { name } = request.body
    const id = Snowflake.Snowflake.generate()
    const owner = request.id
    var statement = `insert into community (id, name, slug, owner) values ('${id}', '${name}', '${name}', '${owner}')`
    db.query(statement, (error, dbResult) => {
        if (error) {
            response.send(utils.createError(error))
        } else {
            statement = `insert into member (id, community, user, role) select '${Snowflake.Snowflake.generate()}', '${id}', '${owner}', role.id from role where role.name = 'Community Admin'`
            db.query(statement, (error, data) => {
                if (error) {
                    response.send(utils.createError(error))
                } else {
                    statement = `select * from community where id = '${id}'`
                    db.query(statement, (error, data) => {
                        if (error) {
                            response.send(utils.createError(error))
                        } else {
                            response.send(utils.createSuccess(data))
                        }
                    })
                }
            })
        }
    })
})


// ---------------------------------------
//                  GET
// ---------------------------------------

// To get a community details whose owner is user

router.get('/', (request, response) => {
    const uid = request.id
    const statement = `select u.name as uname, c.id as cid, c.name as cname, c.slug, c.created_at, c.updated_at from user u inner join community c where u.id = '${uid}' and c.owner = u.id`
    db.query(statement, (error, dbResult) => {
        if (error) {
            response.send(utils.createError(error))
        } else {
            var data = []
            for (let index = 0; index < dbResult.length; index++) {
                const element = dbResult[index];
                data[index] = {
                    id: element['cid'],
                    name: element['cname'],
                    slug: element['slug'],
                    owner: {
                        id: uid,
                        name: element['uname']
                    },
                    created_at: element['created_at'],
                    updated_at: element['updated_at']
                }
            }
            response.send(utils.createMetaSuccess(data))
        }
    })
})


// ---------------------------------------
//                  GET
// ---------------------------------------

// To get all members of a given community
// Takes community ID instead of name.
router.get('/:cid/members', (request, response) => {
    const { cid } = request.params
    const statement = `select m.id as mid, c.id as cid, u.id as uid, u.name as uname, r.id as rid, r.name as rname, m.created_at
    from member m
    INNER JOIN community c on m.community = c.id
    INNER JOIN user u on m.user = u.id
    INNER JOIN role r on m.role = r.id WHERE c.id = '${cid}'`
    // console.log(statement)
    db.query(statement, (error, data) => {
        if (error) {
            response.send(utils.createError(error))
        } else {
            var dbResult = []
            for (let index = 0; index < data.length; index++) {
                const element = data[index];
                dbResult.push({
                    id: element['mid'],
                    community: element['cid'],
                    user: {
                        id: element['uid'],
                        name: element['uname']
                    },
                    role: {
                        id: element['rid'],
                        name: element['rname']
                    },
                    created_at: element['created_at']
                })
            }
            response.send(utils.createMetaSuccess(dbResult))
        }
    })
})


// ---------------------------------------
//                  GET
// ---------------------------------------

// To get community whose owner is admin

router.get('/me/owner', (request, response) => {
    const uid = request.id
    const statement = `select * from community where owner = '${uid}'`
    db.query(statement, (error, data) => {
        if (error) {
            response.send(utils.createError(error))
        } else {
            response.send(utils.createMetaSuccess(data))
        }
    })
})


// ---------------------------------------
//                  GET
// ---------------------------------------

// To get community where user has joined as member (dont show communities where user is admin) but show where user is moderator

router.get('/me/member', (request, response) => {
    const uid = request.id
    const statement = `select c.id as cid, c.name as cname, c.slug, c.owner as adminId, (SELECT u.name as uname from user u where u.id = c.owner) as owner, 
    c.created_at, c.updated_at from community c 
    INNER JOIN member m on m.community = c.id
    INNER JOIN role r on m.role = r.id
    where m.user = '${uid}' and c.owner != '${uid}'`
    db.query(statement, (error, data) => {
        if (error) {
            response.send(utils.createError(error))
        } else {
            var result = []
            for (let index = 0; index < data.length; index++) {
                const element = data[index];
                result.push({
                    id: element['cid'],
                    name: element['cname'],
                    slug: element['slug'],
                    owner: {
                        id: element['adminId'],
                        name: element['owner'],
                    },
                    
                    created_at: element['created_at'],
                    updated_at: element['updated_at']
                })
            }
            response.send(result)
        }
    })
})

export default router