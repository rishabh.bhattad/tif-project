import express from 'express'
import crypto from 'crypto-js'
import jwt from 'jsonwebtoken'
import secret from '../config.js'
import Snowflake from '@theinternetfolks/snowflake'
import utils from '../utils.js'
import db from '../db.js'

const router = express.Router()

// ---------------------------------------
//                  POST
// ---------------------------------------

// For Signup

router.post('/auth/signup', (request, response) => {
    const { name, email, password } = request.body
    var statement = `insert into user (id, name, email, password) values ('${Snowflake.Snowflake.generate()}', '${name}', '${email}', '${crypto.SHA256(password)}')`
    // console.log(statement)

    db.query(statement, (error, data) => {
        if (error) {
            utils.createError(error) 
        } else {
            statement = `select id, name, email from user where email = '${email}'`
            db.query(statement, (error, dbResult) => {
                if (error) {
                    utils.createError(error) 
                } else {
                    response.send(utils.createSuccess(dbResult))
                }
            })

        }
    })
})

// ---------------------------------------
//                  POST
// ---------------------------------------

// To SignIn

router.post('/auth/signin', (request, response) => {
    const { email, password } = request.body
    const statement = `SELECT * FROM user WHERE email='${email}' AND password='${crypto.SHA256(password)}'`
    db.query(statement, (error, dbResult) => {
        if (error) {
            response.send(utils.createError(error))
        } else {
            if (dbResult.length == 0) {
                response.send(utils.createError('No such user found'))
            } else {
                const user = dbResult[0];
                const token = jwt.sign({id: user['id']}, secret)
                response.cookie('token', token, {path: '/'})
                const data = {
                    status: 'true',
                    content: {
                        data: {
                            name: user['name'],
                            email: user['email'],
                            created_at: user['created_at']
                        }
                    },
                    meta: {
                        access_token: token
                    }

                }
                response.send(data)
            }
        }
    })
})

// ---------------------------------------
//                  GET
// ---------------------------------------

// Extra route to clear cookies and signout
router.get('/signout', (request, response) => {
    response.clearCookie('token')
    response.send('You are logged out')
})

// ---------------------------------------
//                  GET
// ---------------------------------------

router.get('/auth/me', (request, response) => {
    const statement = `select name, email, created_at from user where id = '${request.id}'`
    db.query(statement, (error, data) => {
        if (error) {
            response.send(utils.createError(error))
        } else {
            response.send(utils.createSuccess(data))
        }
    })
})

export default router