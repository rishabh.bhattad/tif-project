import express from 'express'
import Snowflake from '@theinternetfolks/snowflake'
import utils from '../utils.js'
import db from '../db.js'

const router = express.Router()

// ---------------------------------------
//                  POST
// ---------------------------------------

// To add member in community. Only admin has access to remove and add. While moderator has only access to add.

router.post('/', (request, response) => {
    // user is id of the user we want to add to the community
    const { community, user, role } = request.body

    // login id of the current user logged in
    const uid = request.id

    // below statement does not work for moderator
    // var statement = `select id from community where owner = '${owner}'`

    //Check if community and the user id has admin/moderator privilages
    var statement = `select name from role r INNER JOIN member m on r.id = m.role 
    where m.user = '${uid}' AND m.community = '${community}' AND r.name in ('Community Admin', 'Community Moderator')`
    console.log(statement)
    db.query(statement, (error, data) => {
        // Anyone can have multiple communities as admin or 0
        if (error) {
            response.send(utils.createError(error))
        } else {
            if (data.length == 0) {
                response.send(utils.createError('NOT_ALLOWED_ACCESS'))
            } else {
                const mid = Snowflake.Snowflake.generate();
                statement = `insert into member (id, community, user, role) values ('${mid}', '${community}', '${user}', '${role}')`
                db.query(statement, (error, data) => {
                    if (error) {
                        response.send(utils.createError(error))
                    } else {
                        response.send(utils.createSuccess({
                            id: mid,
                            community: community,
                            user: user,
                            role: role
                        }))
                    }
                })
            }

            // var flag = 0;
            // // console.log(data)
            // for (let index = 0; index < data.length; index++) {
            //     if (community == data[index]['id']) {
            //         flag = 1;
            //     }           
            // }
            // if (flag == 0) {
            //     response.send(utils.createError('NOT_ALLOWED_ACCESS'))
            // } else {
            //     const mid = Snowflake.Snowflake.generate();
            //     statement = `insert into member (id, community, user, role) values ('${mid}', '${community}', '${user}', '${role}')`
            //     db.query(statement, (error, data) => {
            //         if (error) {
            //             response.send(utils.createError(error))
            //         } else {
            //             response.send(utils.createSuccess({
            //                 id: mid,
            //                 community: community,
            //                 user: user,
            //                 role: role
            //             }))
            //         }
            //     })
            // }
        }
    })
})


// ---------------------------------------
//                  DELETE
// ---------------------------------------

// To delete member from community using member id.. and not UID. Only admin has access to remove. While moderator has access to add.

router.delete('/:mid', (request, response) => {
    const owner = request.id
    const { mid } = request.params
    // console.log(mid)

    // to check whether the user logged in is the admin of the community from where member is being deleted as well as member does exist
    var statement = `select m.id from member m inner join community c on m.community = c.id where m.id = '${mid}' and c.owner = '${owner}'`
    
    db.query(statement, (error, data) => {
        if (error) {
            response.send(utils.createError(error))
        } else {
            if (data.length == 0) {
                response.send(utils.createError("NOT_ALLOWED_ACCESS"))
            } else {
                statement = `delete from member where id = '${mid}'`
                db.query(statement, (error, dbResult) => {
                    if (error) {
                        response.send(utils.createError(error))
                    } else {
                        response.send({status: true})
                    }
                })

            }

        }
    })
})

export default router