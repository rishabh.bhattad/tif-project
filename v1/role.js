import express from 'express'
import Snowflake from '@theinternetfolks/snowflake'
import utils from '../utils.js'
import db from '../db.js'


const router = express.Router()

// ---------------------------------------
//                  POST
// ---------------------------------------

// Create roles

router.post('/', (request, response) => {
    const { name } = request.body

    const sf = Snowflake.Snowflake.generate();

    const statement = `insert into role (id, name) values ('${sf}', '${name}')`
    db.query(statement, (error, data) => {
        if (error) {
            const result = {};
            result['status'] = 'false'
            result['error'] = error
            result['hint'] = 'Only roles allowed are Community Admin, Community Moderator, Community User'
            response.send(result)
        } else {

            response.send(utils.createSuccess({
                id: sf,
                name: name
            }))
        }
    })
    
})

// ---------------------------------------
//                  GET
// ---------------------------------------

// Get All roles

router.get('/', (request, response) => {
    const statement = `select * from role`
    db.query(statement, (error, data) => {
        if (error) {
            response.send(utils.createError(error))
        } else {
            response.send(utils.createMetaSuccess(data))   
        }
    })
})

export default router
