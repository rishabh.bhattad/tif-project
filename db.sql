CREATE user "TIF" @"localhost" IDENTIFIED BY "tif";

create database tifProject;

GRANT ALL ON tifProject.* TO 'TIF' @'localhost';

use tifProject;

create table user (
    id VARCHAR(64) PRIMARY KEY,
    name VARCHAR(64),
    email VARCHAR(128) UNIQUE,
    password VARCHAR(64), 
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

create table role (
    id VARCHAR(64) PRIMARY KEY,
    name ENUM('Community Admin', 'Community Member', 'Community Moderator') UNIQUE,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

create table community (
    id VARCHAR(64) PRIMARY KEY,
    name VARCHAR(64),
    slug VARCHAR(255),
    owner VARCHAR(64),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (owner) REFERENCES user(id)
);

create table member (
    id VARCHAR(64) PRIMARY KEY,
    community VARCHAR(64),
    user VARCHAR(64),
    role VARCHAR(64),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user) REFERENCES user(id),
    FOREIGN KEY (community) REFERENCES community(id) ON DELETE CASCADE,
    FOREIGN KEY (role) REFERENCES role(id)
);

-- Imp query to use to check DB

-- select m.id as mid, c.id as cid, c.name as cname, u.id as uid, u.name as uname, r.id as rid, r.name from tifproject.member m 
-- inner join tifproject.community c on m.community = c.id 
-- inner join tifproject.user u on m.user = u.id 
-- inner join tifproject.role r on r.id = m.role;
